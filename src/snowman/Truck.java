/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snowman;

import java.applet.Applet;
import java.awt.*;

/**
 *
 * @author witwicky
 */
public class Truck extends Applet{
    
    @Override
    public void paint(Graphics g) {
        //draw the base rectangle frame
        g.drawRect(100, 100, 250, 100);
        
        //draw the two wheels
        g.drawOval(120, 185, 30, 30);
        g.drawOval(300, 185, 30, 30);
        
        //draw the windshield
        g.drawLine(100, 100, 150, 50);
        
        //draw roof of cabin
        g.drawLine(150, 50, 225, 50);
        
        //draw rear of cabin
        g.drawLine(225, 50, 225, 100);
        
        //draw the first window
        g.drawLine(110, 95, 150, 55);
        g.drawLine(150, 55, 175, 55);
        g.drawLine(175, 55, 175, 95);
        g.drawLine(110, 95, 175, 95);
        
        //draw the second window
        g.drawLine(180, 55, 220, 55);
        g.drawLine(180, 55, 180, 95);
        g.drawLine(220, 55, 220, 95);
        g.drawLine(180, 95, 220, 95);
    }
    
}
